//
//  PopularMovie.swift
//  MovieList
//
//  Created by Halcyon Mobile SRL on 05.05.2021.
//

import Foundation

struct PopularMovie: Identifiable {
    let id: UUID
    let title: String
    let releaseYear: Int
    let imdbScore: Double
    let imageName: String
}
