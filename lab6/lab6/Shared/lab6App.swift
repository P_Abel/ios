//
//  lab6App.swift
//  Shared
//
//  Created by Halcyon on 10.05.2022.
//

import SwiftUI

@main
struct lab6App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
