//
//  ContentView.swift
//  Shared
//
//  Created by Halcyon on 10.05.2022.
//

import SwiftUI

struct ContentView: View {
    let movies: [PopularMovie] = [
        PopularMovie(id: UUID(), title: "The Hobbit", releaseYear: 2012, imdbScore: 7.8, imageName: "the-hobbit"),
        PopularMovie(id: UUID(), title: "The Hobbit 2", releaseYear: 2013, imdbScore: 7.8, imageName: "the-hobbit"),
        PopularMovie(id: UUID(), title: "The Hobbit 3", releaseYear: 2014, imdbScore: 7.8, imageName: "the-hobbit"),
        PopularMovie(id: UUID(), title: "The Hobbit 4", releaseYear: 2015, imdbScore: 7.8, imageName: "the-hobbit")
    ]
    
    var body: some View {
        VStack(alignment: .leading) {
            Text("My movies")
                .font(.title)
                .fontWeight(.heavy)
                .padding()
            Color.blue.frame(width: UIScreen.main.bounds.width / 2, height: 10, alignment: .leading)
            List(movies, id: \.id) { movie in ContentCell(movie: movie).listRowSeparator(.hidden)
            }
            .listStyle(PlainListStyle())
            .navigationTitle("My movies")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
