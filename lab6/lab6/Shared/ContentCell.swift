//
//  ContentCell.swift
//  lab6
//
//  Created by Halcyon on 10.05.2022.
//

import SwiftUI

struct ContentCell: View {
    let movie: PopularMovie
    
    var body: some View {
        HStack(alignment: .top) {
            Image(movie.imageName)
                .resizable()
                .frame(width: 100, height: 100)
                .cornerRadius(5)
                .padding()
            VStack(alignment: .leading) {
                Text(movie.title)
                    .font(.title)
                    .fontWeight(.bold)
                Text(String(movie.releaseYear))
                    .foregroundColor(.gray)
            }.padding()
            Spacer()
            Button { } label: {
                Image(systemName: "star").foregroundColor(.gray)
            }.padding()
        }
        .background(.white)
        .cornerRadius(5)
        .padding(2)
        .shadow(color: .gray, radius: 5)
    }
}
