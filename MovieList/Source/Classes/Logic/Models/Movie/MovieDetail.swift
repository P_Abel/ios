//
//  Movie.swift
//  MovieList
//
//  Created by Halcyon Mobile SRL on 20.04.2021.
//

import Foundation

struct MovieDetail: Hashable {
    let id: Int
    let title: String
    let description: String
    let imdbScore: Double
    let imdbRatingCount: Int
    let tomatoScore: Int
    let releaseYear: Int
    let ageGroup: String
    let length: String
    let language: String
    let categories: [String]
    let imageURL: URL?
}
