//
//  MovieService.swift
//  MovieList
//
//  Created by Halcyon Mobile SRL on 22.04.2021.
//

import Foundation

class MovieService {
    private let networkService = NetworkService()
    
    func loadPopularMovies(completion: @escaping (NetworkResult<PopularMoviesDTO>) -> Void) {
        let request = PopularMoviesRequest()
        networkService.performRequest(request: request, completion: completion)
    }
    
    func loadMovieDetails(completion: @escaping (NetworkResult<MovieDetailDTO>) -> Void, id: Int) {
        let request = MovieDetailRequest(id: id)
        networkService.performRequest(request: request, completion: completion)
    }
}
