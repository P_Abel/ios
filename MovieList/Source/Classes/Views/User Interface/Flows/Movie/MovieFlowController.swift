//
//  MovieFlowController.swift
//  MovieList
//
//  Created by Halcyon Mobile on 01.04.2021.
//

import UIKit

final class MovieFlowController: NavigationFlowController {
    
    private let movieService: MovieService
    
    init(from parent: FlowController?, movieService: MovieService) {
        self.movieService = movieService
        super.init(from: parent)
    }
    
    required init(from parent: FlowController?) {
        fatalError("init(from:) has not been implemented")
    }
    
    override var firstScreen: UIViewController {
        let movieListViewModel = MovieListViewModel(movieService: movieService)
        movieListViewModel.flowDelegate = self
        let movieListViewController = MovieListViewController(viewModel: movieListViewModel)
        return movieListViewController
    }
}

extension MovieFlowController: MovieListFlowDelegate {
    
    func didSelectMovie(with id: Int) {
        let detailViewModel = MovieDetailViewModel(movieId: id, movieService: movieService)
        navigationController.pushViewController(MovieDetailViewController(viewModel: detailViewModel), animated: true)
    }
    
    func showFavoriteAlert(movieTitle: String, favorite: Bool) {
        let alert = UIAlertController(title: "\(movieTitle) \(favorite ? L10n.addedToFavorites : L10n.removedFromFavorites).", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: L10n.ok, style: .default, handler: nil))
        navigationController.present(alert, animated: true)
    }
}
