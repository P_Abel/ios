//
//  MovieCellViewModel.swift
//  MovieList
//
//  Created by Halcyon Mobile on 20.05.2021.
//

import Foundation

final class MovieCellViewModel {
    
    private let movie: PopularMovie
    
    let title: String
    let releaseYear: String
    let categories: String
    let language: String
    let rating: String
    let imageUrl: URL?

    init(movie: PopularMovie) {
        self.movie = movie
        
        self.title = movie.title
        self.releaseYear = String(movie.releaseYear)
        self.categories = movie.categories.joined(separator: ", ")
        self.language = movie.language
        self.rating = String(movie.imdbScore)
        self.imageUrl = movie.imageURL
    }
}
