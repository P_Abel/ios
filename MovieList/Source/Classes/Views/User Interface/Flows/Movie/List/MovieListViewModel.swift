//
//  ListViewModel.swift
//  MovieList
//
//  Created by Halcyon Mobile on 20.05.2021.
//

import Foundation

protocol MovieListFlowDelegate: AnyObject {
    func didSelectMovie(with id: Int)
    func showFavoriteAlert(movieTitle: String, favorite: Bool)
}

final class MovieListViewModel {
    
    weak var flowDelegate: MovieListFlowDelegate?
    
    var cellViewModels: [MovieCellViewModel] = []
    
    private var movies: [PopularMovie] = []
    
    private let movieService: MovieService
    
    init(movieService: MovieService) {
        self.movieService = movieService
    }
    
    func loadPopularMovies(completion: @escaping (Bool) -> Void) {
        movieService.loadPopularMovies { [weak self] (result: NetworkResult<PopularMoviesDTO>) in
            guard let self = self else { return }
            
            switch result {
            case .success(let popularMoviesDto):
                self.movies = popularMoviesDto.results.compactMap { PopularMovie($0) }
                self.cellViewModels = self.movies.compactMap { movie in
                    let cellViewModel = MovieCellViewModel(movie: movie)
                    return cellViewModel
                }
                completion(true)
            case .failure:
                completion(false)
            }
        }
    }
    
    func movieSelected(at index: Int) {
        flowDelegate?.didSelectMovie(with: movies[index].id)
    }
}
