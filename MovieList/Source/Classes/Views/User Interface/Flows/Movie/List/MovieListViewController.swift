//
//  MovieListViewController.swift
//  MovieList
//
//  Created by Halcyon Mobile on 01.04.2021.
//

import UIKit

private enum LayoutValues {
    static let headerInSectionHeight: CGFloat = 16
}

final class MovieListViewController: UIViewController {
    
    private let viewModel: MovieListViewModel
    
    // MARK: - Private properties
    
    private let tableView = UITableView()
    private let loadingIndicator = UIActivityIndicatorView()
    
    // MARK: - Init
    
    init(viewModel: MovieListViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = Asset.Colors.greyExtraLight.color
        
        setupLoadingView()
        setupTableView()
        
        loadPopularMovies()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    // MARK: - Network Request
    
    private func loadPopularMovies() {
        tableView.isHidden = true
        loadingIndicator.isHidden = false
        
        viewModel.loadPopularMovies { [weak self] loadedSuccessfully in
            guard let self = self else { return }
            
            self.loadingIndicator.isHidden = true
            if loadedSuccessfully {
                self.tableView.isHidden = false
                self.tableView.reloadData()
            } else {
                let alert = UIAlertController(title: L10n.error, message: L10n.checkInternet, preferredStyle: .actionSheet)
                alert.addAction(UIAlertAction(title: L10n.retry, style: .default, handler: { _ in
                    self.loadPopularMovies()
                }))
                alert.addAction(UIAlertAction(title: L10n.cancel, style: .cancel))
                self.present(alert, animated: true)
            }
        }
    }
    
    // MARK: - Loading View
    
    private func setupLoadingView() {
        loadingIndicator.startAnimating()
        view.addSubview(loadingIndicator)
        
        loadingIndicator.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            loadingIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            loadingIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
    }
    
    // MARK: - Table View
    
    private func setupTableView() {
        loadingIndicator.isHidden = true
        tableView.isHidden = false
        
        tableView.backgroundColor = view.backgroundColor
        tableView.separatorStyle = .none
        
        tableView.dataSource = self
        tableView.register(MovieCell.self, forCellReuseIdentifier: MovieCell.cellIdentifier)
        tableView.tableFooterView = UIView()
        tableView.delegate = self
        
        let headerView = MovieListHeaderView(title: L10n.myMovies)
        headerView.translatesAutoresizingMaskIntoConstraints = false
        tableView.tableHeaderView = headerView
        tableView.tableHeaderView?.layoutIfNeeded()
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tableView)
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            
            headerView.topAnchor.constraint(equalTo: tableView.topAnchor),
            headerView.centerXAnchor.constraint(equalTo: tableView.centerXAnchor),
            headerView.widthAnchor.constraint(equalTo: tableView.widthAnchor)
        ])
    }
}

// MARK: - TableView delegates

extension MovieListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.cellViewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: MovieCell.cellIdentifier, for: indexPath)
                as? MovieCell else {
            fatalError("Could not dequeue MovieCell")
        }
        cell.viewModel = viewModel.cellViewModels[indexPath.row]
        return cell
    }
}

extension MovieListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.movieSelected(at: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return LayoutValues.headerInSectionHeight
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
}
