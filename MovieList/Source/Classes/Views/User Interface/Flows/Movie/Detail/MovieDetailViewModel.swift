//
//  DetailViewModel.swift
//  MovieList
//
//  Created by Halcyon Mobile on 20.05.2021.
//

import Foundation

final class MovieDetailViewModel {
    
    private let movieId: Int
    private let movieService: MovieService
    
    var imageUrl: URL?
    var title: String?
    var releaseYear: String?
    var ageGroup: String?
    var length: String?
    var language: String?
    var description: String?
    var imbdRating: String?
    var tomatoRating: String?
    
    var homePageUrl: URL?
    
    private var movieDetail: MovieDetail? {
        didSet {
            guard let movieDetail = movieDetail else { return }
            
            self.imageUrl = movieDetail.imageURL
            self.title = movieDetail.title
            self.releaseYear = String(movieDetail.releaseYear)
            self.ageGroup = movieDetail.ageGroup
            self.length = movieDetail.length
            self.language = movieDetail.language
            self.description = movieDetail.description
            self.imbdRating = String(movieDetail.imdbScore)
            self.tomatoRating = String(movieDetail.tomatoScore)
            self.homePageUrl = movieDetail.homePageURL
        }
    }
    
    init(movieId: Int, movieService: MovieService) {
        self.movieId = movieId
        self.movieService = movieService
    }
    
    func loadMovieDetails(completion: @escaping (Bool) -> Void) {
        movieService.loadMovieDetails(id: movieId) { [weak self] (result: NetworkResult<MovieDetailDto>) in
            guard let self = self else { return }

            switch result {
            case .success(let movieDto):
                self.movieDetail = MovieDetail(movieDto)
                if self.movieDetail != nil {
                    completion(true)
                } else {
                    completion(false)
                }
            case .failure:
                completion(false)
            }
        }
    }
}
