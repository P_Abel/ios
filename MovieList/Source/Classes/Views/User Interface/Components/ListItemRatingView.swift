//
//  ListItemRatingView.swift
//  MovieList
//
//  Created by Halcyon Mobile on 21.04.2021.
//

import UIKit

private enum LayoutValues {
    static let fontSize: CGFloat = 12
    static let cornerRadius: CGFloat = 4
}

class MovieListItemRatingView: UILabel {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = Asset.Colors.blue.color
        layer.masksToBounds = true
        layer.cornerRadius = LayoutValues.cornerRadius
        layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
        textAlignment = .center
        textColor = .white
        font = UIFont.boldSystemFont(ofSize: LayoutValues.fontSize)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var intrinsicContentSize: CGSize {
        let defaultSize = super.intrinsicContentSize
        return CGSize(width: defaultSize.width + 2 * CGFloat.oneAndHalfPadding,
                      height: defaultSize.height + 2 * CGFloat.smallPadding)
    }
}
