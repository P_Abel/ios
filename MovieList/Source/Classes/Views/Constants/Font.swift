//
//  Font.swift
//  project-skeleton
//
//  Created by Halcyon Mobile on 18/07/16.
//  Copyright © 2016 Halcyon Mobile. All rights reserved.
//

import UIKit

// TODO: Use own font sizes and font family
enum FontSize: CGFloat {
    /// 11
    case extraSmall     = 11
    /// 14
    case mediumSmall    = 14
    /// 16
    case medium         = 16
    /// 24
    case large          = 24
}

private struct FontFamily {

    enum OpenSans: String, FontConvertible {
        case light            = "OpenSans-Light"
        case regular          = "OpenSans"
        case bold             = "OpenSans-Semibold"
    }
}

struct Font {

    static func light(size: FontSize) -> UIFont {
        return FontFamily.OpenSans.light.font(size: size)
    }

    static func regular(size: FontSize) -> UIFont {
        return FontFamily.OpenSans.regular.font(size: size)
    }

    static func bold(size: FontSize) -> UIFont {
        return FontFamily.OpenSans.bold.font(size: size)
    }
}

protocol FontConvertible {
    func font(size: FontSize) -> UIFont!
}

extension FontConvertible where Self: RawRepresentable, Self.RawValue == String {
    func font(size: FontSize) -> UIFont! {
        return UIFont(name: self.rawValue, size: size.rawValue)
    }
}
