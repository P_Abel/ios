//
//  NetworkRequest.swift
//  NetworkLayer
//
//  Created by Szabados Zoltan on 27/11/2018.
//  Copyright © 2018 Szabados Zoltan. All rights reserved.
//

import UIKit

open class NetworkRequest<A: Codable> {
    open private(set) var baseUrl: String = BaseURLManager.baseURL
    open private(set) var urlSuffix: String = ""
    open private(set) var method: HTTPMethod = .get
    open private(set) var params: [AnyHashable: Any]?
    open private(set) var headers: [String: String]? = RequestHeaderManager.headers

    public var isLogging = false

    init(baseUrl: String = BaseURLManager.baseURL, urlSuffix: String = "", method: HTTPMethod = .get, params: [AnyHashable: Any]? = nil, headers: [String: String]? = RequestHeaderManager.headers) {
        self.baseUrl = baseUrl
        self.urlSuffix = urlSuffix
        self.method = method
        self.params = params
        self.headers = headers
    }
}
