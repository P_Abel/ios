//
//  HTTPMethod.swift
//  NetworkLayer
//
//  Created by Szabados Zoltan on 3/10/17.
//  Copyright © 2017 Szabados Zoltan. All rights reserved.
//

import Foundation

public enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case delete = "DELETE"
    case patch = "PATCH"
}
