//
//  MovieDetailRequest.swift
//  MovieList
//
//  Created by Halcyon on 05.04.2022.
//

import Foundation

class MovieDetailRequest: NetworkRequest<MovieDetailDTO> {
    init(id: Int) {
        super.init(urlSuffix: "\(APIConstants.Path.movieDetail)/\(id)")
    }
}
