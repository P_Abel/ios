//
//  PopularMoviesRequest.swift
//  MovieList
//
//  Created by Halcyon Mobile SRL on 05.05.2021.
//

import Foundation

class PopularMoviesRequest: NetworkRequest<PopularMoviesDTO> {
    init() {
        super.init(urlSuffix: "\(APIConstants.Path.popularMovies)")
    }
}
