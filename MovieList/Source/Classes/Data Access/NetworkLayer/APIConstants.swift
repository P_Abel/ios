//
//  APIConstants.swift
//  MovieList
//
//  Created by Halcyon Mobile SRL on 22.04.2021.
//

import Foundation

enum APIConstants {
    static let baseURL = "https://api.themoviedb.org/3"
    static let token = "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI4ODllNDY3MmQ5NDhjNWY3YzM3OGVmMGMyNTY2ZGMwYyIsInN1YiI6IjYwNDY1YjE3ZmFiM2ZhMDA3NmNmOWE1ZCIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.FoLAs5l_0c6uLtH1VEIbeC6WjhgYnaEHloB-3OR0owU"
    static let imageBaseURL = "https://image.tmdb.org/t/p/w500"
    
    enum Path {
        static let popularMovies = "/movie/popular"
        static let movieDetail = "/movie"
    }
}
