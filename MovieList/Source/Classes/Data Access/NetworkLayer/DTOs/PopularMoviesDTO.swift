//
//  PopularMoviesDTO.swift
//  MovieList
//
//  Created by Halcyon Mobile SRL on 22.04.2021.
//

import Foundation

struct PopularMoviesDTO: Codable {
    let results: [PopularMovieDTO]
}

struct PopularMovieDTO: Codable {
    let id: Int
    let title: String
    let imdbScore: Double
    let releaseDate: String
    let language: String
    let poster: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case title = "original_title"
        case imdbScore = "vote_average"
        case releaseDate = "release_date"
        case language = "original_language"
        case poster = "poster_path"
    }
}

// MARK: - PopularMovie mapper

extension PopularMovie {
    init?(_ dto: PopularMovieDTO) {
        guard let releaseYear = Int(String(dto.releaseDate.prefix(4))),
              let poster = dto.poster else {
            return nil
        }
        
        self.id = dto.id
        self.title = dto.title
        self.releaseYear = releaseYear
        self.imdbScore = dto.imdbScore
        self.imageURL = URL(string: APIConstants.imageBaseURL + poster)
    }
}
