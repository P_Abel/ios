//
//  MovieDetailDTO.swift
//  MovieList
//
//  Created by Halcyon Mobile SRL on 22.04.2021.
//

import Foundation

struct MovieDetailDTO: Codable {
    let id: Int
    let title: String
    let imdbScore: Double
    let releaseDate: String
    let language: String
    let poster: String?
    let genres: [MovieGenre]
    let description: String
    let lengthMinutes: Int
    let imbdRatingCount: Int
    let adult: Bool
    
    enum CodingKeys: String, CodingKey {
        case id
        case title = "original_title"
        case imdbScore = "vote_average"
        case releaseDate = "release_date"
        case language = "original_language"
        case poster = "poster_path"
        case genres = "genres"
        case description = "overview"
        case lengthMinutes = "runtime"
        case imbdRatingCount = "vote_count"
        case adult = "adult"
    }
}

struct MovieGenre: Codable {
    let id: Int
    let name: String
}

// MARK: - MovieDetail mapper
        
extension MovieDetail {
    init?(_ dto: MovieDetailDTO) {
        guard let releaseYear = Int(String(dto.releaseDate.prefix(4))),
              let poster = dto.poster else {
            return nil
        }
        
        self.id = dto.id
        self.title = dto.title
        self.releaseYear = releaseYear
        self.imdbScore = dto.imdbScore
        self.imageURL = URL(string: APIConstants.imageBaseURL + poster)
        self.description = dto.description
        self.language = dto.language.prefix(1).capitalized +                  dto.language.dropFirst()

        var lengthMinutes = dto.lengthMinutes
        let lengthHours = lengthMinutes / 60
        lengthMinutes %= 60
        self.length = "\(lengthHours)h \(lengthMinutes)min"
        
        self.imdbRatingCount = dto.imbdRatingCount
        self.tomatoScore = dto.imbdRatingCount
        self.ageGroup = dto.adult ? "Adult" : "Family"
        self.categories = dto.genres.map {$0.name}
    }
}
