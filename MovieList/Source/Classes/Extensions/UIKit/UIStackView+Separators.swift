//
//  UIStackView+Separators.swift
//  MovieList
//
//  Created by Halcyon Mobile on 26.03.2021.
//

import UIKit

extension UIStackView {
    func addHorizontalSeparators(color: UIColor) {
        for index in (1..<arrangedSubviews.count).reversed() {
            let separator = UIView()
            separator.backgroundColor = color
            insertArrangedSubview(separator, at: index)
            separator.widthAnchor.constraint(equalToConstant: 1).isActive = true
            separator.heightAnchor.constraint(equalTo: heightAnchor).isActive = true
            separator.translatesAutoresizingMaskIntoConstraints = false
        }
    }
}
