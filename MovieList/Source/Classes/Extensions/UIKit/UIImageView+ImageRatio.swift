//
//  UIImageView+ImageRatio.swift
//  MovieList
//
//  Created by Halcyon Mobile on 26.03.2021.
//

import UIKit

extension UIImageView {
    
    var imageRatio: CGFloat? {
        guard let height = image?.size.height, let width = image?.size.width else {
            return nil
        }
        return height / width
    }
}
