//
//  MovieCell.swift
//  MovieList
//
//  Created by Noemi Kalman on 21.03.2022.
//

import UIKit

private enum LayoutValues {
    enum Shadow {
        static let opacity: Float = 0.1
        static let offset = CGSize(width: 2, height: 3)
        static let radius: CGFloat = 5
    }
    
    static let cornerRadius: CGFloat = 12.0
    static let imageWidth: CGFloat = 140
    static let imageHeight: CGFloat = 150
    static let titleFontSize: CGFloat = 18
    static let titleNumberOfLines = 3
    static let releaseYearFontSize: CGFloat = 12
    static let languageDescriptionFontSize: CGFloat = 12
    static let languageFontSize: CGFloat = 12
    static let padding: CGFloat = 12
}

protocol MovieCellDelegate: AnyObject {
    func didAddToFavorites(for movie: Movie)
}

class MovieCell: UITableViewCell {
    
    static let cellIdentifier = "movieCell"
    
    weak var delegate: MovieCellDelegate?
    
    // MARK: - Private properties
    
    private let cardView = UIView()
    private let titleLabel = UILabel()
    private let yearLabel = UILabel()
    private let previewImageView = UIImageView()
    private let favoriteButton = UIButton()
    
    var movie: Movie? {
        didSet {
            titleLabel.text = movie!.title
            yearLabel.text = String(movie!.year)
            previewImageView.image = UIImage(named: movie!.coverName)
        }
    }
    
    // MARK: - Init
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        initUI()
        initConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - UI
    
    private func initUI() {
        contentView.backgroundColor = UIColor(red: 212, green: 212, blue: 212, alpha: 1)
        
        cardView.backgroundColor = .white
        cardView.layer.cornerRadius = LayoutValues.cornerRadius
        cardView.layer.shadowColor = UIColor.black.cgColor
        cardView.layer.shadowOpacity = LayoutValues.Shadow.opacity
        cardView.layer.shadowOffset = LayoutValues.Shadow.offset
        cardView.layer.shadowRadius = LayoutValues.Shadow.radius
        
        titleLabel.text = movie?.title
        titleLabel.font = UIFont.boldSystemFont(ofSize: 30)
        titleLabel.numberOfLines = 3
        cardView.addSubview(titleLabel)
        
        if let existingMovie = movie {
            yearLabel.text = String(existingMovie.year)
        }
        yearLabel.font = UIFont.systemFont(ofSize: 10)
        yearLabel.textColor = .gray
        cardView.addSubview(yearLabel)
        
        previewImageView.contentMode = .scaleAspectFill
        previewImageView.layer.cornerRadius = LayoutValues.cornerRadius
        previewImageView.layer.masksToBounds = true
        if let existingMovie = movie {
            previewImageView.image = UIImage(named: existingMovie.coverName)
        }
        cardView.addSubview(previewImageView)
        
        favoriteButton.setImage(UIImage(systemName: "star"), for: .normal)
        favoriteButton.setImage(UIImage(systemName: "star.fill"), for: .selected)
        favoriteButton.tintColor = .gray
        favoriteButton.addTarget(self, action: #selector(favoriteButtonPressed), for: .touchUpInside)
        cardView.addSubview(favoriteButton)
        
        contentView.addSubview(cardView)
    }
    
    private func initConstraints() {
        [cardView, previewImageView, titleLabel, yearLabel, favoriteButton].forEach { view in
            view.translatesAutoresizingMaskIntoConstraints = false
        }
        
        NSLayoutConstraint.activate([
            cardView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: .padding),
            cardView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: .padding2x),
            cardView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -.padding2x),
            cardView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -.padding),
            
            previewImageView.leadingAnchor.constraint(equalTo: cardView.leadingAnchor, constant: .padding),
            previewImageView.topAnchor.constraint(equalTo: cardView.topAnchor, constant: .padding),
            previewImageView.widthAnchor.constraint(equalToConstant: LayoutValues.imageWidth),
            previewImageView.heightAnchor.constraint(equalToConstant: LayoutValues.imageHeight),
            previewImageView.bottomAnchor.constraint(equalTo: cardView.bottomAnchor, constant: -.padding),
             
            titleLabel.leftAnchor.constraint(equalTo: previewImageView.rightAnchor, constant: .padding),
            titleLabel.topAnchor.constraint(equalTo: cardView.topAnchor, constant: .padding),
            titleLabel.trailingAnchor.constraint(equalTo: cardView.trailingAnchor, constant: -.padding),
            
            yearLabel.leftAnchor.constraint(equalTo: previewImageView.rightAnchor, constant: .padding),
            yearLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: .padding),
            yearLabel.trailingAnchor.constraint(equalTo: cardView.trailingAnchor, constant: -.padding),
            
            favoriteButton.topAnchor.constraint(equalTo: cardView.topAnchor, constant: .padding),
            favoriteButton.trailingAnchor.constraint(equalTo: cardView.trailingAnchor, constant: -.padding)
        ])
    }
    
    // MARK: - User interaction
    
    @objc private func favoriteButtonPressed() {
        if let movie = movie {
            delegate?.didAddToFavorites(for: movie)
        }
    }
}
