//
//  IllustrationView.swift
//  Workeepr
//
//  Created by Hanna Kovacs on 27/10/2020.
//

import UIKit

final class IllustrationView: UIView {

    // MARK: - Private properties

    private lazy var logoImageView = UIImageView(image: Asset.Images.swift.image)

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)

        initUI()
        initConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - UI

    private func initUI() {
        backgroundColor = .white

        logoImageView.contentMode = .scaleAspectFit
        addSubview(logoImageView)
    }

    private func initConstraints() {
        logoImageView.translatesAutoresizingMaskIntoConstraints = false

        let constraints = [
            logoImageView.topAnchor.constraint(equalTo: topAnchor),
            logoImageView.bottomAnchor.constraint(equalTo: bottomAnchor),
            logoImageView.leadingAnchor.constraint(equalTo: leadingAnchor),
            logoImageView.trailingAnchor.constraint(equalTo: trailingAnchor)
        ]
        NSLayoutConstraint.activate(constraints)
    }
}
