//
//  RatingView.swift
//  MovieList
//
//  Created by Halcyon Mobile SRL on 18.03.2021.
//

import UIKit

protocol RatingViewDelegate: AnyObject {
    func onTap()
}

private enum LayoutValues {
    static let imageSize: CGFloat = 30
    static let titleFontSize: CGFloat = 18
    static let detailsFontSize: CGFloat = 12
}

class RatingView: UIView, UIGestureRecognizerDelegate {

    weak var delegate: RatingViewDelegate? {
        didSet {
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onTap))
            tapGesture.delegate = self
            addGestureRecognizer(tapGesture)
        }
    }

    // MARK: - Private properties

    private let imageView = UIImageView()
    private let titleLabel = UILabel()
    private let detailsLabel = UILabel()

    // MARK: - Public properties

    var image: UIImage? {
        didSet {
            imageView.image = image
        }
    }
    var title: String? {
        didSet {
            titleLabel.text = title
        }
    }
    var details: String? {
        didSet {
            detailsLabel.text = details
        }
    }

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)

        initUI()
        initConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - UI

    private func initUI() {
        addSubview(imageView)

        titleLabel.font = UIFont.systemFont(ofSize: LayoutValues.titleFontSize)
        titleLabel.textAlignment = .center
        addSubview(titleLabel)

        detailsLabel.font = UIFont.systemFont(ofSize: LayoutValues.detailsFontSize)
        detailsLabel.textColor = Asset.Colors.grey.color
        detailsLabel.textAlignment = .center
        addSubview(detailsLabel)
    }

    private func initConstraints() {
        imageView.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        detailsLabel.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: topAnchor),
            imageView.centerXAnchor.constraint(equalTo: centerXAnchor),
            imageView.heightAnchor.constraint(equalToConstant: LayoutValues.imageSize),
            imageView.widthAnchor.constraint(equalToConstant: LayoutValues.imageSize),
            titleLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: .padding),
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            detailsLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor),
            detailsLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            detailsLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            detailsLabel.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }

    // MARK: - User interaction

    @objc private func onTap() {
        delegate?.onTap()
    }
}
