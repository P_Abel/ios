//
//  CategoryView.swift
//  MovieList
//
//  Created by Halcyon Mobile SRL on 18.03.2021.
//

import UIKit

private enum LayoutValues {
    static let cornerRadius: CGFloat = 4
    static let horizontalPadding: CGFloat = 6
    static let verticalPadding: CGFloat = 2
    static let fontSize: CGFloat = 12
}

final class CategoryLabel: UILabel {

    override init(frame: CGRect) {
        super.init(frame: frame)

        backgroundColor = Asset.Colors.greyLight.color
        layer.masksToBounds = true
        layer.cornerRadius = LayoutValues.cornerRadius
        textAlignment = .center
        font = UIFont.systemFont(ofSize: LayoutValues.fontSize)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override var intrinsicContentSize: CGSize {
        let defaultSize = super.intrinsicContentSize
        return CGSize(width: defaultSize.width + 2 * LayoutValues.horizontalPadding,
                      height: defaultSize.height + 2 * LayoutValues.verticalPadding)
    }
}
