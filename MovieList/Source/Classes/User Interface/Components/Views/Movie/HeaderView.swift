//
//  TableHeaderView.swift
//  MovieList
//
//  Created by Halcyon Mobile SRL on 20.04.2021.
//

import UIKit

private enum LayoutValues {
    static let titleFontSize: CGFloat = 32
    static let separatorViewHeight: CGFloat = 12
    static let separatorViewWidthMultiplier: CGFloat = 0.44
}

class MovieListHeaderView: UIView {
    
    // MARK: - Private properties
    
    private let title: String
    
    private let titleLabel = UILabel()
    private let separatorView = UIView()
    
    // MARK: - Init
    
    init(title: String) {
        self.title = title
        
        super.init(frame: CGRect())
        
        initUI()
        initConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - UI
    
    private func initUI() {
        titleLabel.font = UIFont.boldSystemFont(ofSize: LayoutValues.titleFontSize)
        titleLabel.text = title
        addSubview(titleLabel)
        
        separatorView.backgroundColor = Asset.Colors.blue.color
        addSubview(separatorView)
    }
    
    private func initConstraints() {
        [titleLabel, separatorView].forEach { view in
            view.translatesAutoresizingMaskIntoConstraints = false
        }
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: .padding3x),
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: .padding2x),
            titleLabel.trailingAnchor.constraint(lessThanOrEqualTo: trailingAnchor, constant: -.padding2x),
            separatorView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: .oneAndHalfPadding),
            separatorView.leadingAnchor.constraint(equalTo: leadingAnchor),
            separatorView.heightAnchor.constraint(equalToConstant: LayoutValues.separatorViewHeight),
            separatorView.widthAnchor.constraint(equalTo: widthAnchor,
                                                 multiplier: LayoutValues.separatorViewWidthMultiplier),
            separatorView.bottomAnchor.constraint(lessThanOrEqualTo: bottomAnchor)
        ])
    }
}
