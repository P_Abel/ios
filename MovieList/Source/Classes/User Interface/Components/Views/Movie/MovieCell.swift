//
//  MovieCell.swift
//  MovieList
//
//  Created by Halcyon Mobile SRL on 20.04.2021.
//

import UIKit
import Kingfisher

private enum LayoutValues {
    static let cornerRadius: CGFloat = 12.0
    static let imageWidth: CGFloat = 140
    static let imageHeight: CGFloat = 150
    static let titleFontSize: CGFloat = 18
    static let titleNumberOfLines = 3
    static let releaseYearFontSize: CGFloat = 12
    static let languageDescriptionFontSize: CGFloat = 12
    static let languageFontSize: CGFloat = 12
    
    enum Shadow {
        static let opacity: Float = 0.03
        static let offset = CGSize(width: 2, height: 3)
        static let radius: CGFloat = 5
    }
}

protocol MovieCellDelegate: AnyObject {
    func setFavorite(movie: PopularMovie, newValue: Bool)
}

class MovieCell: UITableViewCell {
    
    static let cellIdentifier = "movieCell"
    
    // MARK: - Private properties
    
    private let cardView = UIView()
    private let titleLabel = UILabel()
    private let releaseYearLabel = UILabel()
    private let previewImageView = UIImageView()
    private let ratingView = MovieListItemRatingView()
    private let favoriteButton = UIButton()
    
    private let defaults = UserDefaults.standard
    
    // MARK: - Public properties
    
    weak var delegate: MovieCellDelegate?
    
    var movie: PopularMovie? {
        didSet {
            if let movie = movie {
                titleLabel.text = movie.title
                releaseYearLabel.text = String(movie.releaseYear)
                ratingView.text = String(movie.imdbScore)
                previewImageView.kf.setImage(with: movie.imageURL)
                updateFavoriteButton()
            }
        }
    }
    
    // MARK: - Init
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        initUI()
        initConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - UI
    
    private func initUI() {
        contentView.backgroundColor = Asset.Colors.greyExtraLight.color
        
        cardView.backgroundColor = .white
        cardView.layer.cornerRadius = LayoutValues.cornerRadius
        cardView.layer.shadowColor = UIColor.black.cgColor
        cardView.layer.shadowOpacity = LayoutValues.Shadow.opacity
        cardView.layer.shadowOffset = LayoutValues.Shadow.offset
        cardView.layer.shadowRadius = LayoutValues.Shadow.radius
        
        previewImageView.contentMode = .scaleAspectFill
        previewImageView.layer.cornerRadius = LayoutValues.cornerRadius
        previewImageView.layer.masksToBounds = true
        cardView.addSubview(previewImageView)
        
        titleLabel.font = UIFont.boldSystemFont(ofSize: LayoutValues.titleFontSize)
        titleLabel.numberOfLines = LayoutValues.titleNumberOfLines
        cardView.addSubview(titleLabel)
        
        releaseYearLabel.font = UIFont.systemFont(ofSize: LayoutValues.releaseYearFontSize)
        releaseYearLabel.textColor = Asset.Colors.grey.color
        cardView.addSubview(releaseYearLabel)
        
        cardView.addSubview(ratingView)
        
        favoriteButton.setImage(UIImage(systemName: "star"), for: .normal)
        favoriteButton.setImage(UIImage(systemName: "star.fill"), for: .selected)
        favoriteButton.tintColor = Asset.Colors.grey.color
        favoriteButton.addTarget(self, action: #selector(favoriteButtonPressed), for: .touchUpInside)
        cardView.addSubview(favoriteButton)
        
        contentView.addSubview(cardView)
    }
    
    private func initConstraints() {
        [cardView, previewImageView, titleLabel, releaseYearLabel,
         ratingView, favoriteButton].forEach { view in
            view.translatesAutoresizingMaskIntoConstraints = false
        }
        
        NSLayoutConstraint.activate([
            cardView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: .padding),
            cardView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: .padding2x),
            cardView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -.padding2x),
            cardView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -.padding),
            
            previewImageView.leadingAnchor.constraint(equalTo: cardView.leadingAnchor, constant: .oneAndHalfPadding),
            previewImageView.topAnchor.constraint(equalTo: cardView.topAnchor, constant: .oneAndHalfPadding),
            previewImageView.widthAnchor.constraint(equalToConstant: LayoutValues.imageWidth),
            previewImageView.heightAnchor.constraint(equalToConstant: LayoutValues.imageHeight),
            previewImageView.bottomAnchor.constraint(equalTo: cardView.bottomAnchor, constant: -.oneAndHalfPadding),
            
            titleLabel.topAnchor.constraint(equalTo: cardView.topAnchor, constant: .oneAndHalfPadding),
            titleLabel.leadingAnchor.constraint(equalTo: previewImageView.trailingAnchor, constant: .oneAndHalfPadding),
            titleLabel.trailingAnchor.constraint(lessThanOrEqualTo: favoriteButton.leadingAnchor,
                                                 constant: -.halfPadding),
            
            releaseYearLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: .smallPadding),
            releaseYearLabel.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor),
            releaseYearLabel.trailingAnchor.constraint(lessThanOrEqualTo: cardView.trailingAnchor),
            
            ratingView.leadingAnchor.constraint(equalTo: cardView.leadingAnchor),
            ratingView.topAnchor.constraint(equalTo: cardView.topAnchor, constant: .padding3x),
            ratingView.trailingAnchor.constraint(lessThanOrEqualTo: previewImageView.trailingAnchor,
                                                 constant: -.oneAndHalfPadding),
            
            favoriteButton.topAnchor.constraint(equalTo: cardView.topAnchor, constant: .oneAndHalfPadding),
            favoriteButton.trailingAnchor.constraint(equalTo: cardView.trailingAnchor, constant: -.oneAndHalfPadding)
        ])
    }
    
    private func updateFavoriteButton() {
        if let movie = movie {
            if (defaults.bool(forKey: "\(movie.id)")) {
                favoriteButton.tintColor = Asset.Colors.yellow.color
                favoriteButton.isSelected = true
            } else {
                favoriteButton.tintColor = Asset.Colors.grey.color
                favoriteButton.isSelected = false
            }
        }
    }
    
    // MARK: - User interaction
    
    @objc private func favoriteButtonPressed() {
        if let delegate = delegate, let movie = movie {
            if favoriteButton.isSelected {
                delegate.setFavorite(movie: movie, newValue: false)
            } else {
                delegate.setFavorite(movie: movie, newValue: true)
            }
            updateFavoriteButton()
        }
    }
}
