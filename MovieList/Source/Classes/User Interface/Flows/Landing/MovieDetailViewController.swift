//
//  LoginViewController.swift
//  Workeepr
//
//  Created by Hanna Kovacs on 27/10/2020.
//

import UIKit

final class MovieDetailViewController: UIViewController {

    // MARK: - Private properties
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var coverImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "the-hobbit"))
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 10
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 38, weight: .bold)
        label.text = "The Hobbit: An Unexpected Journey"
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var underlineView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .blue
        return view
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 12, weight: .bold)
        label.text = "The Hobbit: An Unexpected Journey The Hobbit: An Unexpected Journey The Hobbit: An Unexpected Journey The Hobbit: An Unexpected Journey The Hobbit: An Unexpected Journey The Hobbit: An Unexpected Journey The Hobbit: An Unexpected Journey The Hobbit: An Unexpected Journey The Hobbit: An Unexpected Journey The Hobbit: An Unexpected Journey The Hobbit: An Unexpected Journey The Hobbit: An Unexpected Journey"
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var underlineView2: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .lightGray
        return view
    }()
    
    private var infoStackView: UIStackView!
    private var genreStackView: UIStackView!

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        initUI()
        initConstraints()
    }

    // MARK: - UI

    private func initUI() {
        view.backgroundColor = .white

        view.addSubview(scrollView)
        scrollView.addSubview(containerView)
        containerView.addSubview(coverImageView)
        containerView.addSubview(titleLabel)
        containerView.addSubview(underlineView)
        
        infoStackView = createInfoStackView(for: ["2012", "PG-13", "2h 49m", "Eng"])
        containerView.addSubview(infoStackView)
        containerView.addSubview(descriptionLabel)
        genreStackView = createGenreStackView(for: ["Adventure", "Fantasy", "Action", "Popular"])
        containerView.addSubview(genreStackView)
        containerView.addSubview(underlineView2)
        
    }

    private func initConstraints() {
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            
            containerView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            containerView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            containerView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            containerView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            containerView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            containerView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            
            coverImageView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: .padding2x),
            coverImageView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: .padding2x),
            coverImageView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -.padding2x),
            coverImageView.heightAnchor.constraint(equalToConstant: 200),
            
            titleLabel.topAnchor.constraint(equalTo: coverImageView.bottomAnchor, constant: .padding2x),
            titleLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: .padding2x),
            titleLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -.padding2x),
            
            underlineView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: .padding2x),
            underlineView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            underlineView.widthAnchor.constraint(equalToConstant: view.frame.width / 2),
            underlineView.heightAnchor.constraint(equalToConstant: .underlineViewHeight),
            
            infoStackView.topAnchor.constraint(equalTo: underlineView.bottomAnchor, constant: .padding2x),
            infoStackView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: .padding2x),
            infoStackView.trailingAnchor.constraint(lessThanOrEqualTo: containerView.trailingAnchor, constant: -.padding2x),
            
            descriptionLabel.topAnchor.constraint(equalTo: infoStackView.bottomAnchor, constant: .padding2x),
            descriptionLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: .padding2x),
            descriptionLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -.padding2x),
            
            genreStackView.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: .padding2x),
            genreStackView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: .padding2x),
            genreStackView.trailingAnchor.constraint(lessThanOrEqualTo: containerView.trailingAnchor, constant: -.padding2x),
            
            underlineView2.topAnchor.constraint(equalTo: genreStackView.bottomAnchor, constant: .padding2x),
            underlineView2.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            underlineView2.widthAnchor.constraint(equalToConstant: view.frame.width  - 40),
            underlineView2.heightAnchor.constraint(equalToConstant: 1),
            underlineView2.bottomAnchor.constraint(equalTo: containerView.bottomAnchor)
            
        ])
    }
    
    private func createGenreView(for genre: String) -> UIView {
        let genreView = UIView()
        genreView.backgroundColor = UIColor(named: "genre_gray")
        genreView.translatesAutoresizingMaskIntoConstraints = false
        genreView.layer.cornerRadius = 5
        
        let label = UILabel()
        label.text = genre
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 11)
        label.translatesAutoresizingMaskIntoConstraints = false
        
        genreView.addSubview(label)
        
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: genreView.topAnchor, constant: .halfPadding),
            label.leadingAnchor.constraint(equalTo: genreView.leadingAnchor, constant: .halfPadding),
            label.trailingAnchor.constraint(equalTo: genreView.trailingAnchor, constant: -.halfPadding),
            label.bottomAnchor.constraint(equalTo: genreView.bottomAnchor, constant: -.halfPadding)
        ])
        
        return genreView
    }
    
    private func createInfoView(for info: String) -> UIView {
        let infoView = UIView()
        infoView.backgroundColor = UIColor(named: "white")
        infoView.translatesAutoresizingMaskIntoConstraints = false

        let label = UILabel()
        label.text = info
        label.textColor = .gray
        label.font = UIFont.systemFont(ofSize: 11)
        label.translatesAutoresizingMaskIntoConstraints = false
        
        infoView.addSubview(label)
        
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: infoView.topAnchor, constant: .halfPadding),
            label.leadingAnchor.constraint(equalTo: infoView.leadingAnchor, constant: .halfPadding),
            label.trailingAnchor.constraint(equalTo: infoView.trailingAnchor, constant: -.halfPadding),
            label.bottomAnchor.constraint(equalTo: infoView.bottomAnchor, constant: -.halfPadding)
        ])
        
        return infoView
    }
    
    private func createSeparatorView() -> UIView {
        let view = UIStackView()
        view.backgroundColor = UIColor(named: "genre_gray")
        let label = UILabel()
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 9)
        label.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(label)
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: view.topAnchor, constant: .halfPadding),
            label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 1),
            label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -1),
            label.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -.halfPadding)
        ])
        return view
    }
    
    private func createGenreStackView(for genres: [String]) -> UIStackView {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.distribution = .fillProportionally
        stackView.spacing = 10
        
        genres.forEach { genre in
            stackView.addArrangedSubview(createGenreView(for: genre))
        }
        
        return stackView
    }
    
    private func createInfoStackView(for infos: [String]) -> UIStackView {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.distribution = .fillProportionally
        stackView.spacing = 10
        
        infos.indices.forEach { index in
            if (index > 0) {
                stackView.addArrangedSubview(createSeparatorView())
            }
            stackView.addArrangedSubview(createInfoView(for: infos[index]))
        }
        
        return stackView
    }
    
}

private extension CGFloat {
    static let coverImageHeight: CGFloat = 100
    static let underlineViewHeight: CGFloat = 10
}
