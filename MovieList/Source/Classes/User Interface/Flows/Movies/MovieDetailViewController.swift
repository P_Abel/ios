//
//  LoginViewController.swift
//  Workeepr
//
//  Created by Hanna Kovacs on 27/10/2020.
//

import UIKit

final class MovieDetailViewController: UIViewController {

    // MARK: - Private properties
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var coverImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "the-hobbit"))
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 100
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 38, weight: .light)
        label.text = "The Hobbit: An Unexpected Journey"
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var underlineView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .red
        return view
    }()
    
    private var genreStackView: UIStackView!

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        initUI()
        initConstraints()
    }

    // MARK: - UI

    private func initUI() {
        view.backgroundColor = .white

        view.addSubview(containerView)
        containerView.addSubview(coverImageView)
        containerView.addSubview(titleLabel)
        containerView.addSubview(underlineView)
        
        genreStackView = createGenreStackView(for: ["Adventure", "Fantasy", "Action", "Popular"])
        containerView.addSubview(genreStackView)
    }

    private func initConstraints() {
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            containerView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            containerView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            containerView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            
            coverImageView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: .padding2x),
            coverImageView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: .padding2x),
            coverImageView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -.padding2x),
            
            titleLabel.topAnchor.constraint(equalTo: coverImageView.bottomAnchor, constant: .padding2x),
            titleLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: .padding2x),
            titleLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -.padding2x),
            
            underlineView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: .padding2x),
            underlineView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            underlineView.widthAnchor.constraint(equalToConstant: view.frame.width / 2),
            underlineView.heightAnchor.constraint(equalToConstant: .underlineViewHeight),
            
            genreStackView.topAnchor.constraint(equalTo: underlineView.bottomAnchor, constant: .padding2x),
            genreStackView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: .padding2x),
            genreStackView.trailingAnchor.constraint(lessThanOrEqualTo: containerView.trailingAnchor, constant: -.padding2x)
        ])
    }
    
    private func createGenreView(for genre: String) -> UIView {
        let genreView = UIView()
        genreView.backgroundColor = UIColor(named: "genre_gray")
        genreView.translatesAutoresizingMaskIntoConstraints = false
        
        let label = UILabel()
        label.text = genre
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 11)
        label.translatesAutoresizingMaskIntoConstraints = false
        
        genreView.addSubview(label)
        
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: genreView.topAnchor, constant: .halfPadding),
            label.leadingAnchor.constraint(equalTo: genreView.leadingAnchor, constant: .halfPadding),
            label.trailingAnchor.constraint(equalTo: genreView.trailingAnchor, constant: -.halfPadding),
            label.bottomAnchor.constraint(equalTo: genreView.bottomAnchor, constant: -.halfPadding)
        ])
        
        return genreView
    }
    
    private func createGenreStackView(for genres: [String]) -> UIStackView {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.distribution = .fillProportionally
        stackView.spacing = 10
        
        genres.forEach { genre in
            stackView.addArrangedSubview(createGenreView(for: genre))
        }
        
        return stackView
    }
}

private extension CGFloat {
    static let coverImageHeight: CGFloat = 100
    static let underlineViewHeight: CGFloat = 10
}
