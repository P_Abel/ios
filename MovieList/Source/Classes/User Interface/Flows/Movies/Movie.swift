//
//  Movie.swift
//  MovieList
//
//  Created by Halcyon on 22.03.2022.
//

import Foundation

class Movie {
    var year: Int
    var title: String
    var coverName: String
    var favorite: Bool
    
    init(year: Int, title: String, coverName: String) {
        self.year = year
        self.title = title
        self.coverName = coverName
        self.favorite = false
    }
}
