//
//  MovieListViewController.swift
//  MovieList
//
//  Created by Noemi Kalman on 21.03.2022.
//

import UIKit

private enum LayoutValues {
    static let headerInSectionHeight: CGFloat = 16
}

final class MovieListViewController: UIViewController {

    // MARK: - Private properties
    
    private let tableView = UITableView()
    
    private var movies: [Movie] = [
        Movie(year: 2020, title: "Harry Potter", coverName: "poster-for-movie"),
        Movie(year: 2017, title: "Hobbit", coverName: "the-hobbit"),
        Movie(year: 2001, title: "Shrek", coverName: "shrek1"),
        Movie(year: 2007, title: "Cover", coverName: "cover1")
    ]
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor(red: 212, green: 212, blue: 212, alpha: 1)
        
        setupTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    // MARK: - Table View
    
    private func setupTableView() {        
        tableView.backgroundColor = view.backgroundColor
        tableView.separatorStyle = .none
        tableView.tableFooterView = UIView()
        tableView.register(MovieCell.self, forCellReuseIdentifier: MovieCell.cellIdentifier)
        tableView.dataSource = self
        tableView.delegate = self
        
        let headerView = MovieListHeaderView(title: "My Movies")
        headerView.translatesAutoresizingMaskIntoConstraints = false
        tableView.tableHeaderView = headerView
        tableView.tableHeaderView?.layoutIfNeeded()
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tableView)
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            
            headerView.topAnchor.constraint(equalTo: tableView.topAnchor),
            headerView.centerXAnchor.constraint(equalTo: tableView.centerXAnchor),
            headerView.widthAnchor.constraint(equalTo: tableView.widthAnchor)
        ])
    }
}

extension MovieListViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: MovieCell.cellIdentifier) as? MovieCell else {
             return UITableViewCell()
        }
        cell.movie = movies[safe: indexPath.row]
        cell.delegate = self
        return cell
    }
}

extension MovieListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return LayoutValues.headerInSectionHeight
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        navigationController?.pushViewController(MovieDetailViewController(), animated: true)
    }
}

// MARK: - MovieCellDelegate
extension MovieListViewController: MovieCellDelegate {
    func didAddToFavorites(for movie: Movie) {
        let alert = UIAlertController(title: "Movie titled \(movie.title)", message: "\(movie.title) is now added to favorites", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}
