//
//  MovieFlowController.swift
//  MovieList
//
//  Created by Halcyon Mobile SRL on 01.04.2021.
//

import UIKit

final class MovieFlowController: NavigationFlowController {
    
    private let movieService: MovieService
    
    init(from parent: FlowController?, movieService: MovieService) {
        self.movieService = movieService
        super.init(from: parent)
    }
    
    required init(from parent: FlowController?) {
        fatalError("init(from:) has not been implemented")
    }
    
    override var firstScreen: UIViewController {
        let movieListViewController = MovieListViewController(movieService: movieService)
        movieListViewController.flowDelegate = self
        return movieListViewController
    }
}

extension MovieFlowController: MovieListFlowDelegate {
    
    func didSelectMovie(id: Int) {
        // let mockId = 123
        navigationController.pushViewController(DetailViewController(movieService: movieService, movieId: id), animated: true)
    }
}
