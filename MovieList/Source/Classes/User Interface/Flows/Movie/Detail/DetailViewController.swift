//
//  LoginViewController.swift
//  Workeepr
//
//  Created by Hanna Kovacs on 27/10/2020.
//

import UIKit
import Kingfisher

private enum LayoutValues {
    static let imageCornerRadius: CGFloat = 12
    static let titleSeparatorViewHeight: CGFloat = 12
    static let titleSeparatorViewWidthMultiplier: CGFloat = 0.45
    static let descriptionLinesNumber = 12
    static let contentSeparatorViewHeight: CGFloat = 1
    static let titleFontSize: CGFloat = 40
}

final class DetailViewController: UIViewController {

    // MARK: - Private properties

    private let scrollView = UIScrollView()
    private let contentView = UIView()

    private let headerImageView = UIImageView()
    private let titleLabel = UILabel()
    private let titleSeparatorView = UIView()

    private let attributesStackView = UIStackView()
    private let releaseYearAttributeLabel = UILabel()
    private let ageGroupAttributeLabel = UILabel()
    private let lengthAttributeLabel = UILabel()
    private let langaugeAttributeLabel = UILabel()

    private let descriptionLabel = UILabel()

    private let categoriesStackView = UIStackView()
    private let contentSeparatorView = UIView()
    private let ratingsStackView = UIStackView()
    private let imdbRating = RatingView()
    private let rateButton = RatingView()
    private let tomatoRating = RatingView()
    
    private let loadingIndicator = UIActivityIndicatorView()
    
    private let movieService: MovieService
    private let movieId: Int
    
    // MARK: - Init
    
    init(movieService: MovieService, movieId: Int) {
        self.movieService = movieService
        self.movieId = movieId
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        setupLoadingView()
        initContentUI()
        initContentConstraints()
        
        loadMovie()
    }
    
    // MARK: - Network Request
    
    private func loadMovie() {
        scrollView.isHidden = true
        loadingIndicator.isHidden = false
        loadingIndicator.startAnimating()
        
        movieService.loadMovieDetails(completion: { [weak self] (result: NetworkResult<MovieDetailDTO>) in
            guard let self = self else { return }
            switch result {
            case .success(let movieDetailDTO):
                self.loadingIndicator.isHidden = true
                if let movie = MovieDetail(movieDetailDTO) {
                    self.fillViewsWithData(movie)
                }
                self.scrollView.isHidden = false
            case .failure(_):
                self.loadingIndicator.isHidden = true
                let alert = UIAlertController(title: L10n.error,
                                              message: L10n.checkInternet, preferredStyle: .actionSheet)
                alert.addAction(UIAlertAction(title: L10n.retry, style: .default, handler: { _ in
                    self.loadMovie()
                }))
                alert.addAction(UIAlertAction(title: L10n.cancel, style: .cancel))
                self.present(alert, animated: true)
            }
        }, id: movieId)
    }
    
    private func fillViewsWithData(_ movie: MovieDetail) {
        headerImageView.kf.setImage(with: movie.imageURL)
        
        titleLabel.text = movie.title
        
        releaseYearAttributeLabel.text = String(movie.releaseYear)
        ageGroupAttributeLabel.text = movie.ageGroup
        lengthAttributeLabel.text = movie.length
        langaugeAttributeLabel.text = movie.language
        
        descriptionLabel.text = movie.description
        
        categoriesStackView.arrangedSubviews.forEach { $0.removeFromSuperview() }
        movie.categories.forEach { category in
            let categoryLabel = CategoryLabel()
            categoryLabel.text = category
            categoriesStackView.addArrangedSubview(categoryLabel)
        }
        
        imdbRating.title = "\(movie.imdbScore)/10"
        imdbRating.details = String(movie.imdbRatingCount)
        
        tomatoRating.title = "\(movie.tomatoScore)%"
    }
    
    private func presentErrorAlert() {
        let alert = UIAlertController(title: L10n.error,
                                      message: L10n.checkInternet, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: L10n.retry, style: .default, handler: { _ in
            self.loadMovie()
        }))
        alert.addAction(UIAlertAction(title: L10n.cancel, style: .cancel))
        self.present(alert, animated: true)
    }
    
    // MARK: - Loading View
    
    private func setupLoadingView() {
        view.addSubview(loadingIndicator)
        
        loadingIndicator.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            loadingIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            loadingIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
    }
    
    // MARK: - Content UI

    private func initContentUI() {
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        
        loadingIndicator.isHidden = true
        view.addSubview(loadingIndicator)
        
        initHeaderUI()
        initBodyUI()
        initFooterUI()
    }

    private func initHeaderUI() {
        headerImageView.contentMode = .scaleAspectFit
        headerImageView.layer.cornerRadius = LayoutValues.imageCornerRadius
        headerImageView.layer.masksToBounds = true
        contentView.addSubview(headerImageView)

        titleLabel.textAlignment = .center
        titleLabel.textColor = Asset.Colors.blueGrey.color
        titleLabel.font = UIFont.boldSystemFont(ofSize: LayoutValues.titleFontSize)
        titleLabel.lineBreakMode = .byWordWrapping
        titleLabel.numberOfLines = 0
        titleLabel.textAlignment = .natural
        contentView.addSubview(titleLabel)

        titleSeparatorView.backgroundColor = Asset.Colors.blue.color
        contentView.addSubview(titleSeparatorView)
    }

    private func initBodyUI() {
        
        releaseYearAttributeLabel.textColor = Asset.Colors.grey.color
        attributesStackView.addArrangedSubview(releaseYearAttributeLabel)
        
        ageGroupAttributeLabel.textColor = Asset.Colors.grey.color
        attributesStackView.addArrangedSubview(ageGroupAttributeLabel)
    
        lengthAttributeLabel.textColor = Asset.Colors.grey.color
        attributesStackView.addArrangedSubview(lengthAttributeLabel)
    
        langaugeAttributeLabel.textColor = Asset.Colors.grey.color
        attributesStackView.addArrangedSubview(langaugeAttributeLabel)

        attributesStackView.spacing = .padding2x
        attributesStackView.addHorizontalSeparators(color: Asset.Colors.grey.color)
        contentView.addSubview(attributesStackView)

        descriptionLabel.lineBreakMode = .byTruncatingTail
        descriptionLabel.numberOfLines = LayoutValues.descriptionLinesNumber
        contentView.addSubview(descriptionLabel)

        categoriesStackView.spacing = .padding
        contentView.addSubview(categoriesStackView)
    }

    private func initFooterUI() {
        contentSeparatorView.backgroundColor = Asset.Colors.greyLight.color
        contentView.addSubview(contentSeparatorView)

        ratingsStackView.distribution = .fillEqually
        contentView.addSubview(ratingsStackView)

        imdbRating.image = Asset.Images.star.image
        ratingsStackView.addArrangedSubview(imdbRating)

        rateButton.image = Asset.Images.starOutline.image
        rateButton.title = L10n.rateButtonLabel
        ratingsStackView.addArrangedSubview(rateButton)

        tomatoRating.image = Asset.Images.tomato.image
        tomatoRating.details = L10n.tomatometer
        ratingsStackView.addArrangedSubview(tomatoRating)
    }

    // MARK: - Content Constraints

    private func initContentConstraints() {
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        contentView.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: view.topAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor),

            contentView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            contentView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            contentView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            contentView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            contentView.widthAnchor.constraint(equalTo: scrollView.widthAnchor)
        ])

        initHeaderConstraints()
        initBodyConstraints()
        initFooterConstraints()
    }

    private func initHeaderConstraints() {
        headerImageView.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleSeparatorView.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            headerImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: .padding2x),
            headerImageView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: .padding2x),
            headerImageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -.padding2x),
            headerImageView.heightAnchor.constraint(equalTo: headerImageView.widthAnchor,
                                                    multiplier: headerImageView.imageRatio ?? 1),

            titleLabel.topAnchor.constraint(equalTo: headerImageView.bottomAnchor, constant: .padding2x),
            titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: .padding2x),
            titleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -.padding2x),

            titleSeparatorView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: .oneAndHalfPadding),
            titleSeparatorView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            titleSeparatorView.widthAnchor.constraint(equalTo: contentView.widthAnchor,
                                                      multiplier: LayoutValues.titleSeparatorViewWidthMultiplier),
            titleSeparatorView.heightAnchor.constraint(equalToConstant: LayoutValues.titleSeparatorViewHeight)
        ])
    }

    private func initBodyConstraints() {
        [attributesStackView, releaseYearAttributeLabel, ageGroupAttributeLabel,
         lengthAttributeLabel, langaugeAttributeLabel, descriptionLabel, categoriesStackView].forEach { view in
            view.translatesAutoresizingMaskIntoConstraints = false
        }

        NSLayoutConstraint.activate([
            attributesStackView.topAnchor.constraint(equalTo: titleSeparatorView.bottomAnchor, constant: .padding2x),
            attributesStackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: .padding2x),
            attributesStackView.trailingAnchor.constraint(lessThanOrEqualTo: contentView.trailingAnchor,
                                                          constant: -.padding2x),

            descriptionLabel.topAnchor.constraint(equalTo: attributesStackView.bottomAnchor, constant: .padding3x),
            descriptionLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: .padding2x),
            descriptionLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -.padding2x),

            categoriesStackView.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: .padding2x),
            categoriesStackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: .padding2x),
            categoriesStackView.trailingAnchor.constraint(lessThanOrEqualTo: contentView.trailingAnchor,
                                                          constant: -.padding2x)
        ])
    }

    private func initFooterConstraints() {
        contentSeparatorView.translatesAutoresizingMaskIntoConstraints = false
        ratingsStackView.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            contentSeparatorView.topAnchor.constraint(equalTo: categoriesStackView.bottomAnchor, constant: .padding3x),
            contentSeparatorView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: .padding2x),
            contentSeparatorView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -.padding2x),
            contentSeparatorView.heightAnchor.constraint(equalToConstant: LayoutValues.contentSeparatorViewHeight),

            ratingsStackView.topAnchor.constraint(equalTo: contentSeparatorView.bottomAnchor, constant: .padding3x),
            ratingsStackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: .padding2x),
            ratingsStackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -.padding2x),
            ratingsStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -.padding2x)
        ])
    }
}
