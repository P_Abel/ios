//
//  PopularMovie.swift
//  MovieList
//
//  Created by Halcyon Mobile on 05.05.2021.
//

import Foundation

struct PopularMovie {
    let id: Int
    let title: String
    let releaseYear: Int
    let imdbScore: Double
    let language: String
    let imageURL: URL?
    let categories: [String]
}
