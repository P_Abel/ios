//
//  DBError.swift
//  NetworkLayer
//
//  Created by Szabados Zoltan on 3/13/17.
//  Copyright © 2017 Szabados Zoltan. All rights reserved.
//

import Foundation

public protocol CustomBackendErrorRepresentable {
    var code: Int { get }
    var message: String? { get }
    init(code: Int, message: String?)
}

public struct CustomBackendError: Codable, CustomBackendErrorRepresentable {
    public let code: Int
    public let message: String?
    
    public init(code: Int, message: String?) {
        self.code = code
        self.message = message
    }
}

struct CustomBackendErrorContainer: Codable {
    let error: CustomBackendError
}

public enum NetworkError: Swift.Error {
    case noConnectivity
    case failedParsing
    case timeout
    
    /// For 400 HTTP status code
    case badRequest(CustomBackendErrorRepresentable?)
    /// For 401 HTTP status code
    case unauthorized(CustomBackendErrorRepresentable?)
    /// For 403 HTTP status code
    case forbidden(CustomBackendErrorRepresentable?)
    /// For 404 HTTP status code
    case missingResource(CustomBackendErrorRepresentable?)
    /// For 405 HTTP status code
    case methodNotAllowed(CustomBackendErrorRepresentable?)
    /// For 406 HTTP status code
    case notAcceptable(CustomBackendErrorRepresentable?)
    /// For 409 HTTP status code
    case conflict(CustomBackendErrorRepresentable?)
    
    /// For 5xx HTTP status codes
    case serverFailure
    case custom(CustomBackendErrorRepresentable)
    case other
}

public struct ErrorParser {
    
    public func error(httpStatusCode: Int, customBackendError: CustomBackendErrorRepresentable? = nil) -> NetworkError? {
        switch httpStatusCode {
        case 200..<300:
            return nil
        case 400:
            return .badRequest(customBackendError)
        case 401:
            return .unauthorized(customBackendError)
        case 403:
            return .forbidden(customBackendError)
        case 404:
            return .missingResource(customBackendError)
        case 405:
            return .methodNotAllowed(customBackendError)
        case 406:
            return .notAcceptable(customBackendError)
        case 409:
            return .conflict(customBackendError)
        default:
            return .other
        }
    }
}
