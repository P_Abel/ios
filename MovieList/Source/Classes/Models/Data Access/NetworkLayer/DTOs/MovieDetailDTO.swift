//
//  MovieDetailDTO.swift
//  MovieList
//
//  Created by Halcyon Mobile on 22.04.2021.
//

import Foundation

struct MovieDetailDto: Codable {
    let id: Int
    let title: String
    let description: String?
    let imdbScore: Double
    let imdbRatingCount: Int
    let releaseDate: String
    let length: Int?
    let language: String
    let genres: [MovieGenre]
    let poster: String?
    let homepage: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case title = "original_title"
        case description = "overview"
        case imdbScore = "vote_average"
        case imdbRatingCount = "vote_count"
        case releaseDate = "release_date"
        case length = "runtime"
        case language = "original_language"
        case genres
        case poster = "poster_path"
        case homepage
    }
}

struct MovieGenre: Codable {
    let id: Int
    let name: String
}

// MARK: - MovieDetail mapper

extension MovieDetail {
    init?(_ dto: MovieDetailDto) {
        guard let description = dto.description else {
            return nil
        }
        
        guard let releaseYear = Int(String(dto.releaseDate.prefix(4))) else {
            return nil
        }
        
        guard var lengthMinutes = dto.length else {
            return nil
        }
        let lengthHours = lengthMinutes / 60
        lengthMinutes %= 60
        let length = "\(lengthHours)h \(lengthMinutes)min"
        
        let language = dto.language.prefix(1).capitalized + dto.language.dropFirst()
        
        let categories = dto.genres.map { $0.name }
        
        guard let poster = dto.poster else {
            return nil
        }
        let imageURL = URL(string: APIConstants.imageBaseURL + poster)
        
        var homePageURL: URL?
        if let dtoHomepage = dto.homepage {
            homePageURL = URL(string: dtoHomepage)
        }
        
        self.init(id: dto.id,
                  title: dto.title,
                  description: description,
                  imdbScore: dto.imdbScore,
                  imdbRatingCount: dto.imdbRatingCount,
                  tomatoScore: 64,
                  releaseYear: releaseYear,
                  ageGroup: "PG-13",
                  length: length,
                  language: language,
                  categories: categories,
                  imageURL: imageURL,
                  homePageURL: homePageURL)
    }
}
