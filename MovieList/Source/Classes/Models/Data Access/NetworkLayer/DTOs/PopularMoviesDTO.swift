//
//  PopularMoviesDTO.swift
//  MovieList
//
//  Created by Halcyon Mobile on 22.04.2021.
//

import Foundation

struct PopularMoviesDTO: Codable {
    let results: [PopularMovieDTO]
}

struct PopularMovieDTO: Codable {
    let id: Int
    let title: String
    let imdbScore: Double
    let releaseDate: String
    let language: String
    let poster: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case title = "original_title"
        case imdbScore = "vote_average"
        case releaseDate = "release_date"
        case language = "original_language"
        case poster = "poster_path"
    }
}

// MARK: - PopularMovie mapper

extension PopularMovie {
    init?(_ dto: PopularMovieDTO) {
        guard let releaseYear = Int(String(dto.releaseDate.prefix(4))) else {
            return nil
        }
        
        let language = dto.language.prefix(1).capitalized + dto.language.dropFirst()
        
        guard let poster = dto.poster else {
            return nil
        }
        let imageUrl = URL(string: APIConstants.imageBaseURL + poster)
        
        self.init(id: dto.id,
                  title: dto.title,
                  releaseYear: releaseYear,
                  imdbScore: dto.imdbScore,
                  language: language,
                  imageURL: imageUrl,
                  categories: ["Adventure", "Fantasy", "Popular"])
    }
}
