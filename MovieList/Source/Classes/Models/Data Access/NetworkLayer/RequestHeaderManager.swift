//
//  RequestHeaderManager.swift
//  NetworkLayer
//
//  Created by Szabados Zoltan on 3/10/17.
//  Copyright © 2017 Szabados Zoltan. All rights reserved.
//

import Foundation

public protocol RequestHeaderManagerProtocol {
    static var headers: [String:String]? { get set }
}

public struct RequestHeaderManager: RequestHeaderManagerProtocol {
    public static var headers: [String:String]? = ["Content-Type": "application/json",
                                                   "charset": "utf-8",
                                                   "Authorization": APIConstants.token]
}
