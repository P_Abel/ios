//
//  NetworkService.swift
//  NetworkLayer
//
//  Created by Szabados Zoltan on 27/11/2018.
//  Copyright © 2018 Szabados Zoltan. All rights reserved.
//

import UIKit

public protocol NetworkServiceProtocol {
    /// Closure for unauthorized response code
    static var unauthorizedClosure: (() -> Void)? { get set }
    /// Closure for forbidden response code
    static var forbiddenClosure: (() -> Void)? { get set }
    /// Configure network debugger (eg: Netfox)
    func configureNetworkDebugger(_ closure: () -> Void)

    /// Perform request
    func performRequest<A: Codable>(request: NetworkRequest<A>, completion: @escaping (NetworkResult<A>) -> ())
}

public class NetworkService: NetworkServiceProtocol {
    /// Closure for unauthorized response code
    public static var unauthorizedClosure: (() -> Void)? = nil

    /// Closure for forbidden response code
    public static var forbiddenClosure: (() -> Void)? = nil

    /// Configure network debugger (eg: Netfox)
    public func configureNetworkDebugger(_ closure: () -> Void) {
        closure()
    }

    public func performRequest<A: Codable>(request: NetworkRequest<A>, completion: @escaping (NetworkResult<A>) -> ()) {
        // Create request
        guard let urlRequest = createRequest(from: request) else {
            assertionFailure("Not a valid url for request: \(request)")
            return
        }

        // Execute request
        let dataTask = URLSession.shared.dataTask(with: urlRequest) { data, response, error in
            guard let httpResponse = response as? HTTPURLResponse else {
                if request.isLogging {
                    print("NetworkService:: Unknown error. Response should be HTTPURLResponse.")
                }
                DispatchQueue.main.async {
                    completion(.failure(NetworkError.badRequest(nil)))
                }
                return
            }
            // Log returned data
            if request.isLogging, let data = data, let returnData = String(data: data, encoding: .utf8) { // Got data. This can happen even if the status code indicates an error.
                print("NetworkService:: URL: \(urlRequest.url!):: RETURNED DATA: \(returnData)")
            }

            // Error handling
            let decoder = JSONDecoder()
            let backendError = try? decoder.decode(CustomBackendErrorContainer.self, from: data ?? Data()) // parse error object if available from the response
            if let networkError = ErrorParser().error(httpStatusCode: httpResponse.statusCode, customBackendError: backendError?.error) {
                if case NetworkError.unauthorized(_) = networkError {
                    if let unauthorizedClosure = NetworkService.unauthorizedClosure {
                        unauthorizedClosure()
                        return // don't call the completion block, if the unauthorized closure is set. It should have a special behavior.
                    }
                }
                if case NetworkError.forbidden(_) = networkError {
                    if let forbiddenClosure = NetworkService.forbiddenClosure {
                        forbiddenClosure()
                        return // don't call the completion block, if the forbidden closure is set. It should have a special behavior.
                    }
                }

                DispatchQueue.main.async {
                    completion(.failure(networkError))
                }
                return
            } else if let backendError = backendError?.error { // got status code 200, but the response is an error
                DispatchQueue.main.async {
                    completion(.failure(NetworkError.custom(backendError)))
                }
                return
            }

            // Get data from server
            do {
                let object = try decoder.decode(A.self, from: data ?? Data())
                DispatchQueue.main.async {
                    completion(.success(object))
                }
            } catch {
                DispatchQueue.main.async {
                    completion(.failure(NetworkError.failedParsing))
                }
            }
        }
        dataTask.resume()
    }

    private func createRequest<A>(from request: NetworkRequest<A>) -> URLRequest? {
        var urlString = "\(request.baseUrl)\(request.urlSuffix)"
        if let params = request.params, (request.method == .get || request.method == .delete) {
            urlString += "?\(queryString(params: params))"
        }

        guard let url = URL(string: urlString) else {
            return nil
        }
        var urlRequest = URLRequest(url: url)

        // set header fields
        if let headers = request.headers {
            if request.isLogging {
                print("NetworkService:: URL: \(urlRequest.url!):: Headers: \(headers))")
            }
            for (key, value) in headers {
                urlRequest.setValue(value, forHTTPHeaderField: key)
            }
        }

        // set HTTP method
        urlRequest.httpMethod = request.method.rawValue

        // set parameters
        if let params = request.params {
            if request.isLogging {
                print("NetworkService:: URL: \(urlRequest.url!):: Parameters: \(queryString(params: params))")
            }

            if request.method == .post || request.method == .put {
                if isJSONContentType(urlRequest: urlRequest) {
                    urlRequest.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
                } else {
                    urlRequest.httpBody = postBody(params: params)
                }
            }
        }

        return urlRequest
    }

    private func isJSONContentType(urlRequest: URLRequest) -> Bool {
        if let contentType = urlRequest.allHTTPHeaderFields?["Content-Type"] {
            return contentType == "application/json"
        }
        return false
    }

    private func queryString(params: [AnyHashable: Any]?) -> String {
        guard let params = params else {
            return ""
        }
        let query = params.map{ "\($0)=\($1)" }.joined(separator: "&")
        return query
    }

    private func postBody(params: [AnyHashable: Any]) -> Data? {
        let data = queryString(params: params).data(using: .utf8)
        return data
    }
}
