//
//  NetworkResult.swift
//  NetworkLayer
//
//  Created by Szabados Zoltan on 27/11/2018.
//  Copyright © 2018 Szabados Zoltan. All rights reserved.
//

import UIKit

public enum NetworkResult<A> {
    case success(A)
    case failure(Error)
}
