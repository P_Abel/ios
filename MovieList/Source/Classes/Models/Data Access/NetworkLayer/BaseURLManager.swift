//
//  BaseURLManager.swift
//  NetworkLayer
//
//  Created by Szabados Zoltan on 3/10/17.
//  Copyright © 2017 Szabados Zoltan. All rights reserved.
//

import Foundation

public struct BaseURLManager {
    public static var baseURL = APIConstants.baseURL
}
