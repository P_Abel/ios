//
//  MovieDetailRequest.swift
//  MovieList
//
//  Created by Halcyon Mobile on 22.04.2021.
//

import Foundation

class MovieDetailRequest: NetworkRequest<MovieDetailDto> {
    init(with id: Int) {
        super.init(urlSuffix: "\(APIConstants.Path.movieDetail)/\(id)")
    }
}
