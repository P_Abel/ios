//
//  UserDefaultsWrapper.swift
//  MovieList
//
//  Created by Halcyon Mobile on 06.05.2021.
//

import Foundation

protocol UserDefaultsWrapper {
    func setMovieFavorite(movieId: Int, newValue: Bool)
    func isMovieFavorite(movieId: Int) -> Bool
}

extension UserDefaults: UserDefaultsWrapper {
    func setMovieFavorite(movieId: Int, newValue: Bool) {
        set(newValue, forKey: Key.favoriteMovieKey(id: movieId))
    }
    
    func isMovieFavorite(movieId: Int) -> Bool {
        bool(forKey: Key.favoriteMovieKey(id: movieId))
    }
}

private extension UserDefaults {
    enum Key {
        static func favoriteMovieKey(id: Int) -> String {
            "favoriteMovie.\(id)"
        }
    }
}
