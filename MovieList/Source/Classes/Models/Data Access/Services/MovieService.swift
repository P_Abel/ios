//
//  MovieService.swift
//  MovieList
//
//  Created by Halcyon Mobile on 22.04.2021.
//

import Foundation

class MovieService {
    private let networkService = NetworkService()
    
    func loadMovieDetails(id: Int, completion: @escaping (NetworkResult<MovieDetailDto>) -> Void) {
        let request = MovieDetailRequest(with: id)
        networkService.performRequest(request: request, completion: completion)
    }
    
    func loadPopularMovies(completion: @escaping (NetworkResult<PopularMoviesDTO>) -> Void) {
        let request = PopularMoviesRequest()
        networkService.performRequest(request: request, completion: completion)
    }
}
