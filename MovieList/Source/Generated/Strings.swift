// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Strings

// swiftlint:disable function_parameter_count identifier_name line_length type_body_length
internal enum L10n {
  /// added to favorites
  internal static let addedToFavorites = L10n.tr("Localizable", "added_to_favorites")
  /// Cancel
  internal static let cancel = L10n.tr("Localizable", "cancel")
  /// Please check your internet connection.
  internal static let checkInternet = L10n.tr("Localizable", "check_internet")
  /// Error
  internal static let error = L10n.tr("Localizable", "error")
  /// Language
  internal static let language = L10n.tr("Localizable", "language")
  /// My Movies
  internal static let myMovies = L10n.tr("Localizable", "my_movies")
  /// Ok
  internal static let ok = L10n.tr("Localizable", "ok")
  /// Rate it
  internal static let rateButtonLabel = L10n.tr("Localizable", "rate_button_label")
  /// removed from favorites
  internal static let removedFromFavorites = L10n.tr("Localizable", "removed_from_favorites")
  /// Retry
  internal static let retry = L10n.tr("Localizable", "retry")
  /// TOMATOMETER
  internal static let tomatometer = L10n.tr("Localizable", "tomatometer")
}
// swiftlint:enable function_parameter_count identifier_name line_length type_body_length

// MARK: - Implementation Details

extension L10n {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    let format = BundleToken.bundle.localizedString(forKey: key, value: nil, table: table)
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle = Bundle(for: BundleToken.self)
}
// swiftlint:enable convenience_type
