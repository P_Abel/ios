//: Playground - noun: a place where people can play

//iOS Programozás, választható tantárgy 2020
//1. Labor Playground és bevezetés a Swift nyelvbe

import Foundation
import UIKit

// VARIABLES

// 1. (0.5 p) Declare a constant array favouriteNumbers with values for your favourite numbers.

let favouriteNumbers = [42, 36.5, -2]

// 2. (0.5 p) Declare a dictionary with the name of your colleagues and the "department" he/she is working on.

var colleagueDepartments = ["Barabas Sandor": "development",
                            "Csiki Anna": "development",
                            "Nagy Janos": "design",
                            "Kiss Elemer": "testing"]

// 3. (0.5 p) Declare an array of tuples with the list of your colleagues and their height.
// ex. tuple: ("Big Guy", 201)

var colleagueHeights = [(name: "Barabas Sandor", height: 175), (name: "Nagy Janos", height: 198), (name: "Kis Elemer", height: 163.7)]

// 4. (0.5 p) Order the array created above in ascending order (based on height)
// use `sort`

colleagueHeights.sort { (first, second) -> Bool in
    first.height < second.height
}

// 5. (0.5 p) Group the dictionary by department.
// use the: `Dictionary(grouping: ..., by: ...)` initializer

Dictionary(grouping: colleagueDepartments) { (element) -> String in
    element.value
}

// 6. (0.5 p) Order the tuple based on the heights, if there are entries with the same height, order them by alphabetical order.
// hint: the `sort` function can take two propeties as parameter

colleagueHeights.sort { (first, second) -> Bool in
    if first.height != second.height {
        return first.height < second.height
    }
    return first.name < second.name
}

// 7. (0.5 p) Increment every number by one in favouriteNumbers.
// use `map`

favouriteNumbers.map {
    $0 + 1
}

// 8. (0.5 p) Print out only the name of your colleagues from the dictionary.

print(colleagueDepartments.keys)

// 9. (1 p) Print out the average height of your colleagues.
// use `map` + `reduce`

let sumOfHeights = colleagueHeights.map({ $0.height })
    .reduce(0, { (result, height) in
        result + height
    })
print(sumOfHeights / Double(colleagueHeights.count))

// 10. (1 p) Split the array in half.
// hint: you can access a subarray like this: `someArray[..<n]` or `someArray[n...]`

let count = favouriteNumbers.count
favouriteNumbers[..<Int(count/2)]

// 11. (1 p) Declare the first 10 Fibonacci numbers in a set. (1,1,2,3,...)
// Obviously the number 1 will appear only once in this set (properties of a set)
// write the algorithm, (implement a function like this: `func fibonacci(n: Int) -> Set<Int>`)

func fibonacci(n: Int) -> Set<Int> {
    var result:Set<Int> = []
    var previous = 0
    var current = 1
    for _ in 1...n {
        result.insert(current)
        let newValue = previous + current
        previous = current
        current = newValue
    }
    return result
}
let firstTenFibonacciNumbers = fibonacci(n: 10)

// 12. (1 p) Get the intersect of two sets: the first 10 Fibonacci numbers and the first 10 prime numbers.
// there is a method `intersection`

let firstTenPrimeNumbers = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29]
firstTenFibonacciNumbers.intersection(firstTenPrimeNumbers)

// CONTROL FLOW

// 13. (1 p) Take a number and print the reversed version of it.
// write the algorithm (implement a function like this: `func reverse(number: Int) -> Int`)

func reverse(number: Int) -> Int {
    var number = number
    var result = 0
    while number != 0 {
        result *= 10
        result += number % 10
        number /= 10
    }
    return result
}
print(reverse(number: 123456))

// 14. (1 p) Choose a number and find out if it's palindrome.
// use the solution from above

func isPalindrome(number: Int) -> Bool {
    return number == reverse(number: number)
}
print(isPalindrome(number: 123456))
print(isPalindrome(number: 123321))

// CLASSES AND STRUCTS

// 15. (1 p) Write a struct named Course which will have a String property name and an Int one mark.
// ex. struct Sth { aProperty: Type, ..}

struct Course {
    var name: String
    var mark: Int
}

// 16. (1 p) Create a class named Student which will have a private String property name and a private array of courses which he attends to. Both properties will be initialized in the constructor.

class Student {
    private var name: String
    private var courses: [Course]
    private var type: StudentType
    
    init(name: String, courses: [Course], type: StudentType = .normal) {
        self.name = name
        self.courses = courses
        self.type = type
    }
}

// ENUMS

// 17. (1 p) Create an enum type which will have two cases: normal and scholar.
// ex. enum Sth { case fist, case last }

enum StudentType {
    case normal
    case scholar
}

// 18.( 1 p) Create a String property in the enum (e.g. description) which will return a textual representation of each case (e.g. "This is a scholar student.").
// use the `switch` statement for setting the right textual representation

extension StudentType {
    var description: String {
        get {
            switch self {
            case .normal:
                return "This is a normal student."
            case .scholar:
                return "This is a scholar student."
            }
        }
    }
}

// 19. (1 p) Add this enum as a property to the above mentioned Student class.

// 20. (1 p) Modify the Student class constructor so the type type parameter has a default value of .normal).
// HINT: you can give a default value in the constructor like `init(..., type: Type = .aType)`



// PROTOCOLS

// 21. (1 p) Vehicles can have different properties and functionality.
// All Vehicles:
// • Have a speed at which they move
// • Calculate the duration it will take them to travel a certain distance
// All Vehicles except a Motorcycle
// • Have an amount of Windows
// Only Buses:
// • Have a seating capacity
// Create the following Vehicles types: Car, Bus, Motorcycle. Do not use subclassing, use protocols instead. Create an array over the minimum required protocol and put an instance of every type in it and print the the following text for every item of the array (if it's possible): " *type* has *amount of windows* windows and needs *time* to travel 100 kilometers. "

protocol Vehicle {
    var speed: Double { get set }
    mutating func calculateDuration(distance: Double) -> Double
}

protocol WindowVehicle: Vehicle {
    var numberOfWindows: Int { get }
}

struct Bus: WindowVehicle {
    var speed: Double
    var numberOfWindows: Int
    var seatingCapacity: Int
    
    mutating func calculateDuration(distance: Double) -> Double {
        return distance * 60 / speed
    }
    
    // TODO: implement what's needed and add the needed property(s)
}

// TODO: implement the other types
struct Car: WindowVehicle {
    var speed: Double
    var numberOfWindows: Int
    
    mutating func calculateDuration(distance: Double) -> Double {
        distance / speed
    }
}

struct Motorcycle: Vehicle {
    var speed = 189.15
    
    mutating func calculateDuration(distance: Double) -> Double {
        return distance / speed
    }
}


// TODO: add elements to this array
var vehicles: [Vehicle] = [Bus(speed: 80, numberOfWindows: 10, seatingCapacity: 65),
                           Car(speed: 130, numberOfWindows: 6),
                           Motorcycle(speed: 110)]

let distance = 100.0
for vehicle in vehicles {
    var description = "\(type(of: vehicle)) "
    if let windowVehicle = vehicle as? WindowVehicle {
        description += "has \(windowVehicle.numberOfWindows) windows and "
    }
    var vehicle = vehicle
    description += "needs \(vehicle.calculateDuration(distance: distance)) hours to travel \(distance) kilometers."
    print(description)
}


// EXTENSTIONS

// 22. (1 p) Create Int extensions for:
// • Radian value of a degree (computed property)
// • Array of digits of Int (computed property)
extension Int {
    
    func toRadian () -> Double {
        return Double.pi * Double(self) / 180
    }
    
    // TODO: Array of digits
    var arrayOfDigits: [Int] {
        get {
            var result: [Int] = []
            var temp = reverse(number: self)
            while temp != 0 {
                result.append(temp % 10)
                temp /= 10
            }
            return result
        }
    }
}

// 23. (1 p) Create String extensions for:
// • Check if the string contains the character 'a' or 'A'

extension String {
    func containsA() -> Bool {
        return self.contains("a") || self.contains("A")
    }
}

// 24. (1 p) Create Date extensions for:
// • Check if a date is in the future (computed property)
// • Check if a date is in the past (computed property)
// • Check if a date is in today (computed property)

extension Date{
    func isToday() -> Bool {
        return Calendar.current.isDateInToday(self)
    }
    
    // TODO: is in the future
    func isInFuture() -> Bool {
        return self > Date()
    }
    
    // TODO: is in the past
    func isInPast() -> Bool {
        return self < Date()
    }
}

let formatter = DateFormatter()
formatter.dateFormat = "yyyy/MM/dd"
let date: Date = formatter.date(from: "2018/03/20")!
let today = Date()
// TODO: test the written extension
print(date.isInFuture())
print(date.isInPast())


// CLOSURES

// 25. (1 p) Declare a closure that takes an Integer as an argument and returns if the number is odd or even (using regular syntax and shorthand parameter names)
// ex: `let empty: (String) -> Bool = { $0.isEmpty }` - closure for testing if a string is empty

let isEven: (Int) -> Bool = { $0.isMultiple(of: 2) }

// 26. (1 p) Use the above defined closure to filter out odd numbers from an array of random numbers (use the filter function)
func makeRandomList(n: Int) -> [Int] {
    var result: [Int] = []
    for _ in 0..<n {
        result.append(Int(arc4random_uniform(100) + 1))
    }
    return result
}
// TODO: do the filtering
makeRandomList(n: 10).filter { (number) -> Bool in
    isEven(number)
}


// 27. (1 p) Declare a closure that takes 2 Integers as parameters and returns true if the first argument is larger than the second and false otherwise (using regular syntax and shorthand parameter names)

let isFirstLarger: (Int, Int) -> Bool = { $0 > $1 }
isFirstLarger(56,23)
isFirstLarger(1,2)




